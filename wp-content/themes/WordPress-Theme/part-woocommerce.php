<!-- Begin Banner -->
	<section class="banner" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'banner_productos' ); ?>
			</div>
		</div>
	</section>
<!-- End Banner -->
<!-- Begin Title -->
	<section class="title_products" data-wow-delay="0.5s">
		<div class="row collapse expanded">
			<div class="small-12 columns">
				<h1 class="text-center"><strong>Productos</strong></h1>
			</div>
		</div>
	</section>
<!-- End Title -->
<!-- Begin Content -->
	<section class="content space" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<div class="woocommerce">
					<?php woocommerce_content(); ?>
				</div>
			</div>
		</div>
	</section>
<!-- End Content -->